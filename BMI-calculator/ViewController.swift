//
//  ViewController.swift
//  BMI-calculator
//
//  Created by iosdev on 6.11.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var heightPicker: UIPickerView!
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var calButton: UIButton!
    
    
    let person = Person()
    var heightPickerData: [Int] = [Int]()
  
    @IBOutlet weak var resulLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.heightPicker.delegate = self
        self.heightPicker.dataSource = self
        heightPickerData = Array(1...300)
        
        
    }
   
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return heightPickerData.count
    }
   func pickerView(_ pickerView: UIPickerView,
     titleForRow row: Int,
     forComponent component: Int) -> String?{
    if (component == 0){
        person.height = Double(heightPickerData[row])
    } else {
        person.weight = Double(heightPickerData[row])
    }
            person.name = nameField.text ?? ""
            calButton.isEnabled = !person.name.isEmpty
    return String(heightPickerData[row])
    }
    

    @IBAction func calculateButton(_ sender: UIButton) {
        resulLabel.text =   String(person.bmiCalculator(weight: Float(person.weight), height: Float(person.height)))
       
        
    }
    
    
   
        

       
    }
    



